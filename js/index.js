$(function(){
    clicando_sobre(tag1, main);
    clicando_sobre(tag2, section);
    clicando_sobre(tag3, article);
    voltar();
})

let tag1 = $("#sobre");
let main = $("#main").offset();

let tag2 = $("#palestrantes");
let section = $("#section").offset();

let tag3 = $("#inscricao");
let article = $("#article").offset();


function clicando_sobre(tag, posicao){
    tag.click(event => {
        event.preventDefault();
        $("html, body").animate({
            scrollTop: posicao.top
        },1000)
    })
}

function voltar(){
    $(this).dblclick(() => {
        $("html, body").animate({
            scrollTop: 0
        }, 1000)
    })
}